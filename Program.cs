﻿using System;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] source = new int[9] { 6, 3, 5, 7, 2, 4, 1, 3,7};
            SortedPackage package = new SortedPackage(source);
            package.PrintAll();
        }
    }

    class SortedPackage
    {
        //Алгоритм работает только для сортируемых значений > 0
        int n;
        private int[] b; //здесь хранятся отсортированные элементы
        private int[] c; //c[3] = 2 - означает что в массиве b есть 2 элемента со значением 3
        private int[] d;
        int minValue = int.MaxValue;
        int maxValue = int.MinValue;

        public SortedPackage(int[] a)
        {
            n = a.Length;
            //find max and min
            for (int i = 0; i < n; i++)
            {
                if (a[i] < minValue) minValue = a[i];
                if (a[i] > maxValue) maxValue = a[i];
            }
            //set c
            c = new int[maxValue+1];
            for (int i = 0; i < n; i++)
            {
                c[a[i]]++;
            }
            //set d
            d = new int[maxValue + 1];
            d[minValue - 1] = 1;
            for (int i = minValue; i <= maxValue; i++)
            {
                d[i] = d[i - 1] + c[i - 1] ;
            }
            //sort b
            b = new int[n];
            int[] tmpC = new int[maxValue + 1];
            Array.Copy(c, tmpC, c.Length);
            foreach (int item in a)
            {
                b[d[item] + tmpC[item] -2] = item;
                tmpC[item]--;
            }
        }

        public void Print()
        {
            Console.WriteLine($"Min: {minValue}; Max: {maxValue};");
            foreach (var item in b)
            {
                Console.WriteLine(item);
            }
        }

        public void PrintAll()
        {
            Console.WriteLine($"Min: {minValue}; Max: {maxValue};");
            Console.WriteLine("i:    d:    c:    b:");
            for (int i = 0; i < maxValue+2; i++)
            {
                Console.Write(i + "     ");
                if (d.Length > i)
                {
                    Console.Write(d[i]);
                    Console.Write("     ");
                }
                else
                {
                    Console.Write("      ");
                }

                if (c.Length > i)
                {
                    Console.Write(c[i]);
                    Console.Write("     ");
                }
                else
                {
                    Console.Write("      ");
                } 

                if (b.Length > i)
                {
                    Console.Write(b[i]);
                    Console.Write("     ");
                }
                else
                {
                    Console.Write("      ");
                }
                Console.WriteLine();
            }
        }
    }
}
