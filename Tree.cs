﻿using System;

namespace ADM_Tree
{
    class Program
    {
        static void Main(string[] args)
        {
            Tree<SomeTClass> tree = new Tree<SomeTClass>(new SomeTClass(10));
            tree.Add(new SomeTClass(7));
            tree.Add(new SomeTClass(80));
            tree.Add(new SomeTClass(1));
            tree.Add(new SomeTClass(13));
            //ключ сортировки = -2;
            tree.Add(new SomeTClass(-2));
            //ключ сортировки = 15; личные данные = 666;
            tree.Add(new SomeTClass(15, 666));
            tree.Add(new SomeTClass(8));
            tree.Add(new SomeTClass(9));

            Console.WriteLine(tree);
            //Пример удаления
            tree.Remove(7);
            Console.WriteLine();
            Console.WriteLine(tree);
            //Пример поиска объекта и преобразования его к исходному виду из представления как интерфейс с сохранением личных данных
            Console.WriteLine(tree.GetLeaf(15) as SomeTClass);
        }
    }

    class Tree<T> where T:TreeLeaf
    {
        private TreeLeaf main;

        public Tree()
        {
        }
        public Tree(TreeLeaf treeMain)
        {
            main = treeMain;
        }

        public void Add(TreeLeaf newLeaf)
        {
            if (newLeaf is null) return;
            if(main is null)
            {
                main = newLeaf;
                return;
            }

            if(main.GetLeafValue() < newLeaf.GetLeafValue())
            {
                if(main.RightChild is object)
                {
                    AddTo(main.RightChild, newLeaf);
                }
                else
                {
                    main.RightChild = newLeaf;
                    newLeaf.SetLeaf(main);
                }
            }

            if (main.GetLeafValue() > newLeaf.GetLeafValue())
            {
                if (main.LeftChild is object)
                {
                    AddTo(main.LeftChild, newLeaf);
                }
                else
                {
                    main.LeftChild = newLeaf;
                    newLeaf.SetLeaf(main);
                }
            }

            if (main.GetLeafValue() == newLeaf.GetLeafValue())
            {
                main = newLeaf;
                Console.WriteLine("Корень дерева - заменён.");
            }
        }

        private void AddTo(TreeLeaf parent, TreeLeaf newLeaf)
        {
            if (parent is null) return;
            if (parent.GetLeafValue() < newLeaf.GetLeafValue())
            {
                if (parent.RightChild is object)
                {
                    AddTo(parent.RightChild, newLeaf);
                }
                else
                {
                    parent.RightChild = newLeaf;
                    newLeaf.SetLeaf(parent);
                }
            }

            if (parent.GetLeafValue() > newLeaf.GetLeafValue())
            {
                if (parent.LeftChild is object)
                {
                    AddTo(parent.LeftChild, newLeaf);
                }
                else
                {
                    parent.LeftChild = newLeaf;
                    newLeaf.SetLeaf(parent);
                }
            }

            if (parent.GetLeafValue() == newLeaf.GetLeafValue())
            {
                newLeaf.SetLeaf(parent.Parent);
                parent = newLeaf;
                Console.WriteLine($"Лист дерева со значением {0} - заменён.",parent.GetLeafValue());
            }
        }

        public void Remove(TreeLeaf leafToRemove)
        {
            if (leafToRemove is null) return;
            if (main is null)
            {
                throw new Exception("Невозможно удалить лист из дерева. Дерево не имеет корня.");
            }

            if (main.GetLeafValue() < leafToRemove.GetLeafValue())
            {
                if (main.RightChild is object)
                {
                    RemoveAt(main.RightChild, leafToRemove);
                }
                else
                {
                    string s = "В дереве не найден лист со значением " + leafToRemove.GetLeafValue();
                    throw new Exception(s);
                }
            }

            if (main.GetLeafValue() > leafToRemove.GetLeafValue())
            {
                if (main.LeftChild is object)
                {
                    RemoveAt(main.LeftChild, leafToRemove);
                }
                else
                {
                    string s = "В дереве не найден лист со значением " + leafToRemove.GetLeafValue();
                    throw new Exception(s);
                }
            }

            if (main.GetLeafValue() == leafToRemove.GetLeafValue())
            {
                TreeLeaf left = main.LeftChild;
                main = main.RightChild;
                if(left is object)
                    Add(left);
            }
        }

        public void Remove(int leafToRemoveValue)
        {
            if (main is null)
            {
                throw new Exception("Невозможно удалить лист из дерева. Дерево не имеет корня.");
            }

            if (main.GetLeafValue() < leafToRemoveValue)
            {
                if (main.RightChild is object)
                {
                    RemoveAt(main.RightChild, leafToRemoveValue);
                }
                else
                {
                    string s = "В дереве не найден лист со значением " + leafToRemoveValue;
                    throw new Exception(s);
                }
            }

            if (main.GetLeafValue() > leafToRemoveValue)
            {
                if (main.LeftChild is object)
                {
                    RemoveAt(main.LeftChild, leafToRemoveValue);
                }
                else
                {
                    string s = "В дереве не найден лист со значением " + leafToRemoveValue;
                    throw new Exception(s);
                }
            }

            if (main.GetLeafValue() == leafToRemoveValue)
            {
                TreeLeaf left = main.LeftChild;
                main = main.RightChild;
                if (left is object)
                    AddTo(main,left);
            }
        }

        private void RemoveAt(TreeLeaf parent, TreeLeaf leafToRemove)
        {
            if (parent is null) return;
            if (parent.GetLeafValue() < leafToRemove.GetLeafValue())
            {
                if (parent.RightChild is object)
                {
                    RemoveAt(parent.RightChild, leafToRemove);
                }
                else
                {
                    string s = "В дереве не найден лист со значением " + leafToRemove.GetLeafValue();
                    throw new Exception(s);
                }
            }

            if (parent.GetLeafValue() > leafToRemove.GetLeafValue())
            {
                if (parent.LeftChild is object)
                {
                    RemoveAt(parent.LeftChild, leafToRemove);
                }
                else
                {
                    string s = "В дереве не найден лист со значением " + leafToRemove.GetLeafValue();
                    throw new Exception(s);
                }
            }

            if (parent.GetLeafValue() == leafToRemove.GetLeafValue())
            {
                TreeLeaf left = default;
                TreeLeaf right = default;

                if (parent.LeftChild is object)
                    left = parent.LeftChild;
                if(parent.RightChild is object)
                    right = parent.RightChild;

                if (right is object)
                    parent = right;
                if(left is object)
                    Add(left);
                if(left is null && right is null)
                {
                    if (ReferenceEquals(parent.Parent.RightChild, parent))
                        parent.Parent.RightChild = null;
                    else if(ReferenceEquals(parent.Parent.LeftChild, parent))
                        parent.Parent.LeftChild = null;
                }
            }
        }
        private void RemoveAt(TreeLeaf parent, int leafToRemoveValue)
        {
            if (parent is null) return;
            if (parent.GetLeafValue() < leafToRemoveValue)
            {
                if (parent.RightChild is object)
                {
                    RemoveAt(parent.RightChild, leafToRemoveValue);
                }
                else
                {
                    string s = "В дереве не найден лист со значением " + leafToRemoveValue;
                    throw new Exception(s);
                }
            }

            if (parent.GetLeafValue() > leafToRemoveValue)
            {
                if (parent.LeftChild is object)
                {
                    RemoveAt(parent.LeftChild, leafToRemoveValue);
                }
                else
                {
                    string s = "В дереве не найден лист со значением " + leafToRemoveValue;
                    throw new Exception(s);
                }
            }

            if (parent.GetLeafValue() == leafToRemoveValue)
            {
                TreeLeaf left = default;
                TreeLeaf right = default;
                if (parent.LeftChild is object)
                {
                    left = parent.LeftChild;
                }
                if (parent.RightChild is object)
                {
                    right = parent.RightChild;
                }


                if (right is object)
                {
                    if (ReferenceEquals(parent.Parent.RightChild, parent))
                        parent.Parent.RightChild = right;
                    else if (ReferenceEquals(parent.Parent.LeftChild, parent))
                        parent.Parent.LeftChild = right;

                    if (left is object)
                        AddTo(right, left);
                }
                else if(left is object)
                {
                    if (ReferenceEquals(parent.Parent.RightChild, parent))
                        parent.Parent.RightChild = left;
                    else if (ReferenceEquals(parent.Parent.LeftChild, parent))
                        parent.Parent.LeftChild = left;
                }

                if (left is null && right is null)
                {
                    if (ReferenceEquals(parent.Parent.RightChild, parent))
                        parent.Parent.RightChild = null;
                    else if (ReferenceEquals(parent.Parent.LeftChild, parent))
                        parent.Parent.LeftChild = null;
                }
            }
        }

        public TreeLeaf GetLeaf(int leafID)
        {
            if(main is null)
            {
                throw new Exception("Невозможно получить значение из дерева. Дерево не имеет корня.");
            }

            if(main.GetLeafValue() == leafID)
                return main;

            if (main.GetLeafValue() < leafID)
                return GetLeafRecourse(main.RightChild, leafID);
            if (main.GetLeafValue() > leafID)
                return GetLeafRecourse(main.LeftChild, leafID);

            throw new Exception("Ошибка поиска. Лист с указанным значением отсутствует.");
        }

        private TreeLeaf GetLeafRecourse(TreeLeaf leaf, int leafID)
        {
            if(leaf is null)
                throw new Exception("Ошибка поиска. Лист с указанным значением отсутствует.");

            if (leaf.GetLeafValue() == leafID)
                return leaf;

            if (leaf.GetLeafValue() < leafID)
                return GetLeafRecourse(leaf.RightChild, leafID);
            if (leaf.GetLeafValue() > leafID)
                return GetLeafRecourse(leaf.LeftChild, leafID);

            throw new Exception("Ошибка поиска. Лист с указанным значением отсутствует.");
        }

        public override string ToString()
        {
            Console.WriteLine("Вывод дерева: ");
            Console.WriteLine();

            Console.WriteLine("Корень: " + main.GetLeafValue());
            if (main.LeftChild is object)
                Console.WriteLine(main.GetLeafValue() + " -> " + main.LeftChild.GetLeafValue());
            if (main.RightChild is object)
                Console.WriteLine(main.GetLeafValue() + " -> " + main.RightChild.GetLeafValue());
            Console.WriteLine();

            Console.WriteLine("Левая ветвь: ");
            PrintTree(main.LeftChild);
            Console.WriteLine();

            Console.WriteLine("Правая ветвь: ");
            PrintTree(main.RightChild);
            Console.WriteLine();

            return "Конец вывода дерева...";
        }

        private void PrintTree(TreeLeaf leaf)
        {
            if (leaf is null) return;

            if (leaf.LeftChild is object)
            {
                Console.WriteLine(leaf.GetLeafValue() + " -> " + leaf.LeftChild.GetLeafValue());
                PrintTree(leaf.LeftChild);
            }

            if (leaf.RightChild is object)
            {
                Console.WriteLine(leaf.GetLeafValue() + " -> " + leaf.RightChild.GetLeafValue());
                PrintTree(leaf.RightChild);
            }
        }
    }

    /// <summary>
    /// Интерфейс, гарантирующий наличие необходимых функций у класса поддерживаемого деревом
    /// </summary>
    public interface TreeLeaf
    {
        /// <summary>
        /// Отображает абстрактное значение листа, на самом деле лист может и не быть числовой переменной, просто по этому методу будет проводится сортировка листев.
        /// </summary>
        /// <returns></returns>
        public int GetLeafValue();
        public TreeLeaf Parent { get; set; }
        public TreeLeaf LeftChild { get; set; }
        public TreeLeaf RightChild { get; set; }
        public void SetLeaf(TreeLeaf parent);
    }

    /// <summary>
    /// Некий типизированный класс, который поддерживает все необходимые операции так как наследуется от интерфейса TreeLeaf
    /// Имеет два конструктора:
    /// с одним параметром (указываем только ключ по которому происходит фильтрация в дереве)
    /// с двумя параметрами (ключ сортировки и личное значение класса с тем же успехом это может быть массив любых данных присущих классу)
    /// </summary>
    class SomeTClass : TreeLeaf
    {
        private int LeafValue;
        private int v;
        private SomeTClass parent;
        private SomeTClass leftChild;
        private SomeTClass rightChild;

        public SomeTClass(int value)
        {
            LeafValue = value;
        }
        public SomeTClass(int value,int V)
        {
            LeafValue = value;
            v = V;
        }

        public TreeLeaf Parent
        {
            get { return parent; }
            set { parent = value as SomeTClass; }
        }

        public TreeLeaf LeftChild
        {
            get { return leftChild; }
            set { leftChild = value as SomeTClass; }
        }

        public TreeLeaf RightChild
        {
            get { return rightChild; }
            set { rightChild = value as SomeTClass; }
        }

        public void SetLeaf(TreeLeaf parent)
        {
            this.parent = parent as SomeTClass;
        }

        public int GetLeafValue()
        {
            return LeafValue;
        }

        public override string ToString()
        {
            return "Hi! I am some T class. ('_')/  My local data is:" + v;
        }
    }
}
