﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldTile : MonoBehaviour
{
    public List<WorldTile> neighbours = new List<WorldTile>();
    public WorldTile previous;
    public bool walkable = true;

    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (walkable)
            SetColor(Color.white);
        else
            SetColor(Color.black);
    }

    public void ClearData()
    {
        if (ReferenceEquals(this, ReturnSearch.TargetTile))
        {
            previous = null;
            return;
        }
        if (walkable)
            SetColor(Color.white);
        else
            SetColor(Color.black);

        previous = null;
    }

    private void OnMouseDown()
    {
        FindObjectOfType<ReturnSearch>().FindTheWay(this);
    }



    public void SetColor(Color c)
    {
        spriteRenderer.color = c;
    }
}
