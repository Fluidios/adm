﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnSearch : MonoBehaviour
{
    public static WorldTile TargetTile;
    public int MaxIterations = 10000;
    public WorldTile tile;
    private WorldTile[] world;
    
    private bool pathExist = false;
    private int iterations;
    private Queue<WorldTile> frontier = new Queue<WorldTile>();
    private List<WorldTile> explored = new List<WorldTile>();

    private void Start()
    {
        world = FindObjectsOfType<WorldTile>();
        StartCoroutine(SetTarget());
    }

    public void FindTheWay(WorldTile from)
    {
        pathExist = false;
        for (int i = 0; i < world.Length; i++)
        {
            world[i].ClearData();
        }
        frontier.Enqueue(from);
        StartCoroutine(SearchRoutine());
    }

    public IEnumerator SearchRoutine()
    {
        yield return null;
        while (!pathExist)
        {
            if (frontier.Count > 0)
            {
                WorldTile current = frontier.Dequeue(); //извлекает и возвращает первый элемент очереди
                current.SetColor(Color.blue);
                iterations++;
                if (iterations > MaxIterations)
                {
                    pathExist = true;
                    Debug.LogError("Превышено максимальное количество итераций!");
                }
                //Если данный нод ещё не исследован исследовать его
                if (!explored.Contains(current))
                {
                    current.SetColor(Color.yellow);
                    explored.Add(current);
                }
                //Расширить пространство поиска вокруг текущего нода
                ExpandFrontier(current);
                //Если в обновлённое пространство попал целевой нод - найти путь до него
                if (frontier.Contains(TargetTile))
                {
                    List<WorldTile> Path = GetPathNodes(TargetTile);
                    Debug.Log("Путь найден! Длина = " + Path.Count);

                    for (int i = Path.Count - 1; i >= 0; i--)
                    {
                        Path[i].SetColor(Color.green);
                    }

                    frontier.Clear();
                    explored.Clear();
                    iterations = 0;
                    pathExist = true;
                }
                yield return new WaitForSeconds(0.1f);
                if(!pathExist)current.SetColor(Color.yellow);
            }
            else
            {
                pathExist = true;
            }
        }
    }

    private void ExpandFrontier(WorldTile parent)
    {
        for (int i = 0; i < parent.neighbours.Count; i++)
        {
            if(parent.neighbours[i].walkable)
                if (!explored.Contains(parent.neighbours[i]) && !frontier.Contains(parent.neighbours[i]))
                {
                    parent.neighbours[i].previous = parent;
                    frontier.Enqueue(parent.neighbours[i]); //добавляет элемент в конец очереди
                }
        }
    }

    private List<WorldTile> GetPathNodes(WorldTile end)
    {
        WorldTile tile = end;
        List<WorldTile> path = new List<WorldTile>();
        while(tile.previous != null)
        {
            path.Add(tile);
            tile = tile.previous;
        }
        return path;
    }

    IEnumerator SetTarget()
    {
        yield return new WaitForSeconds(1);
        TargetTile = world[Random.Range(0, world.Length)];
        TargetTile.SetColor(Color.red);
        TargetTile.GetComponent<Collider2D>().enabled = false;
    }
}
